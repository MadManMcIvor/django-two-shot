# from django.shortcuts import render

# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User
# from django.contrib import messages
# from django.shortcuts import redirect


# def register(request):
#     if request.method == "POST":
#         f = UserCreationForm(request.POST)
#         if f.is_valid():
#             f.save()
#             messages.success(request, "Account created successfully")
#             return redirect("register")
#     else:
#         f = UserCreationForm()
#     return render(request, "cadmin/register.html", {"form": f})

# Above is how far I got with this part before checking to see the answer. I'm not sure how I would've gotten without looking at the solution.

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
